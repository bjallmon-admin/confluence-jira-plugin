package it.com.atlassian.confluence.plugins.jira.selenium;

public interface SeleniumTestConstants
{
    public static int ACTION_WAIT = 10000;
    
    public static int PAGE_LOAD_WAIT = 20000;
}
